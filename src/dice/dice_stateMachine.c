/*
 * dice_stateMachine.c
 *
 *  Created on: 29.12.2016
 *      Author: rafal
 */
#include "dice_stateMachine.h"
#include "stdbool.h"
#include "gpio.h"
#include "rcc.h"
#include "rng.h"
#include "lcd/myGFX.h"
#include "dice_display.h"

static void dice_initState(bool isButtonPressed); //funkcja obs�ugi stanu inicjuj�cego
static void dice_waitState(bool isButtonPressed); //funkcja obs�ugi stanu oczekiwania
static void dice_drawState(bool isButtonPressed); //funkcja obs�ugi stanu losowania
static void dice_displayState(bool isButtonPressed); //funkcja obs�ugi stanu wy�wietlania

dice_StateMachineStruct dice;
static uint32_t dice_value;

void dice_initStateMachine(void) {
	dice.states[diceState_init] = dice_initState;
	dice.states[diceState_wait] = dice_waitState;
	dice.states[diceState_draw] = dice_drawState;
	dice.states[diceState_display] = dice_displayState;

	dice.nextState = diceState_init;
	dice.currnetState = diceState_init;
}

void dice_handleStateMachine(void) {
	//przej�cie pomi�dzy stanami
	if (dice.currnetState != dice.nextState)
		dice.currnetState = dice.nextState;
	//obs�uga bierz�cego stanu
	bool button = GPIOI->IDR & GPIO_IDR_IDR_11;

	dice.states[dice.currnetState](button);
}

static void dice_initState(bool isButtonPressed) {
	if (!dice.currentStateEntryDone) {
		lcd_setCursor(75, 50);
		lcd_setTextSize(2);
		lcd_printf("PRESS");
		lcd_setCursor(75, 80);
		lcd_printf("BUTTON");
		lcd_setCursor(0, 230);
		lcd_setTextSize(1);
		lcd_printf("dice_by_Rafaulko");
		dice.currentStateEntryDone = true;
	}
	if (isButtonPressed) {
		dice.nextState = diceState_draw;
		dice.currentStateEntryDone = false;
		diceDisplay_init();
	}
}
static void dice_waitState(bool isButtonPressed) {
	if (isButtonPressed) {
		dice.nextState = diceState_draw;
	}
}

static void dice_drawState(bool isButtonPressed) {
	if (!dice.currentStateEntryDone) {
		dice_value = rng_getRandom();
		dice.currentStateEntryDone = true;
	}
	if (isButtonPressed) {
		dice_value = rng_getRandom();
	} else {
		dice.nextState = diceState_display;
		dice.currentStateEntryDone = false;
	}
}

static void dice_displayState(bool isButtonPressed) {
	uint8_t	v1 = 1 + (dice_value & 0xFF) % 6;
	dice_value >>= 8;
	uint8_t	v2 = 1 + (dice_value & 0xFF) % 6;
	diceDisplay_clear();
	diceDisplay_display(v1 , v2);
	dice.nextState = diceState_wait;
}
