/*
 * dice_display.h
 *
 *  Created on: 29.12.2016
 *      Author: rafal
 */

#ifndef DICE_DICE_DISPLAY_H_
#define DICE_DICE_DISPLAY_H_

void diceDisplay_init(void);
void diceDisplay_clear(void);
void diceDisplay_display(uint8_t value1, uint8_t value2);

#endif /* DICE_DICE_DISPLAY_H_ */
