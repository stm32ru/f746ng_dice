/*
 * dice_stateMachine.h
 *
 *  Created on: 29.12.2016
 *      Author: rafal
 */

#ifndef DICE_DICE_STATEMACHINE_H_
#define DICE_DICE_STATEMACHINE_H_
#include "stdbool.h"

typedef enum dice_stateName_t {
	diceState_init, diceState_wait, diceState_draw, diceState_display, diceState_last
} dice_stateName_t;

typedef void (*dice_state)(bool isButtonPressed);	//definicja typu wska�nika na funkcj� przyjmuj�c� argument typu bool

typedef struct dice_StateMachineStruct {
	dice_state states[diceState_last];			//tablica wska�nik�w do funkcji
	dice_stateName_t nextState;			//nast�pny stan maszyny stan�w
	dice_stateName_t currnetState;		//aktualny stan maszyny stan�w
	bool currentStateEntryDone;			//czy wykonano procedury wej�ciowe stanu
} dice_StateMachineStruct;

void dice_initStateMachine(void);
void dice_handleStateMachine(void);

#endif /* DICE_DICE_STATEMACHINE_H_ */
