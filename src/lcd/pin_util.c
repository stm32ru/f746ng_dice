#include "stm32f7xx.h"
#include "pin_util.h"
#include "gpio.h"
#include "rcc.h"

static inline void delayTicks(uint32_t ticks) {
	while (ticks--) {
		__NOP();
	}
}

//PUBLIC FUNCTIONS
void pins_init() {
	rcc_AHB1ENR_set(
			RCC_AHB1ENR_GPIOAEN | RCC_AHB1ENR_GPIOBEN | RCC_AHB1ENR_GPIOFEN | RCC_AHB1ENR_GPIOGEN | RCC_AHB1ENR_GPIOHEN
					| RCC_AHB1ENR_GPIOIEN);

	gpio_pin_cfg(GPIOA, P0, gpio_mode_output_PP_FS);
	gpio_pin_cfg(GPIOF, P10, gpio_mode_output_PP_FS);
	gpio_pin_cfg(GPIOF, P9, gpio_mode_output_PP_FS);
	gpio_pin_cfg(GPIOF, P8, gpio_mode_output_PP_FS);
	gpio_pin_cfg(GPIOF, P7, gpio_mode_output_PP_FS);
}

void pins_setWrite(void) {
	gpio_pin_cfg(GPIOA, P15, gpio_mode_output_PP_FS);
	gpio_pin_cfg(GPIOB, P4, gpio_mode_output_PP_FS);
	gpio_pin_cfg(GPIOG, P6, gpio_mode_output_PP_FS);
	gpio_pin_cfg(GPIOG, P7, gpio_mode_output_PP_FS);
	gpio_pin_cfg(GPIOH, P6, gpio_mode_output_PP_FS);
	gpio_pin_cfg(GPIOI, P0, gpio_mode_output_PP_FS);
	gpio_pin_cfg(GPIOI, P2, gpio_mode_output_PP_FS);
	gpio_pin_cfg(GPIOI, P3, gpio_mode_output_PP_FS);
}
void pins_setRead(void) {
	gpio_pin_cfg(GPIOA, P15, gpio_mode_in_floating);
	gpio_pin_cfg(GPIOB, P4, gpio_mode_in_floating);
	gpio_pin_cfg(GPIOG, P6, gpio_mode_in_floating);
	gpio_pin_cfg(GPIOG, P7, gpio_mode_in_floating);
	gpio_pin_cfg(GPIOH, P6, gpio_mode_in_floating);
	gpio_pin_cfg(GPIOI, P0, gpio_mode_in_floating);
	gpio_pin_cfg(GPIOI, P2, gpio_mode_in_floating);
	gpio_pin_cfg(GPIOI, P3, gpio_mode_in_floating);
}

void pins_write8bits(uint8_t d) {
	GPIOA->BSRR = GPIO_BSRR_BR_15;
	GPIOB->BSRR = GPIO_BSRR_BR_4;
	GPIOG->BSRR = GPIO_BSRR_BR_6 | GPIO_BSRR_BR_7;
	GPIOH->BSRR = GPIO_BSRR_BR_6;
	GPIOI->BSRR = GPIO_BSRR_BR_0 | GPIO_BSRR_BR_2 | GPIO_BSRR_BR_3;
	if (d & 0x01)
		GPIOI->ODR |= GPIO_ODR_ODR_2;
	if (d & 0x02)
		GPIOA->ODR |= GPIO_ODR_ODR_15;
	if (d & 0x04)
		GPIOG->ODR |= GPIO_ODR_ODR_6;
	if (d & 0x08)
		GPIOB->ODR |= GPIO_ODR_ODR_4;
	if (d & 0x10)
		GPIOG->ODR |= GPIO_ODR_ODR_7;
	if (d & 0x20)
		GPIOI->ODR |= GPIO_ODR_ODR_0;
	if (d & 0x40)
		GPIOH->ODR |= GPIO_ODR_ODR_6;
	if (d & 0x80)
		GPIOI->ODR |= GPIO_ODR_ODR_3;
	WR_STROBE
	;
}

void pins_writeRegister8(uint8_t reg, uint8_t data) {
	CD_COMMAND;
	pins_write8bits(reg);
	CD_DATA;
	pins_write8bits(data);
}

void pins_writeRegister16(uint16_t reg, uint16_t data) {
	CD_COMMAND;
	pins_write8bits(reg >> 8);
	pins_write8bits(reg & 0xFF);
	CD_DATA;
	pins_write8bits(data >> 8);
	pins_write8bits(data & 0xFF);
}

void pins_writeRegister32(uint8_t r, uint32_t d) {
	CS_ACTIVE;
	CD_COMMAND;
	pins_write8bits(r);
	CD_DATA;
	pins_write8bits(d >> 24);
	pins_write8bits(d >> 16);
	pins_write8bits(d >> 8);
	pins_write8bits(d);
	CS_IDLE;
}

uint8_t pins_read8bits(void) {
	uint8_t result = 0;
	RD_ACTIVE;
	delayTicks(10);

	result |= (GPIOI->IDR & GPIO_ODR_ODR_2) << 0;
	result |= (GPIOA->IDR & GPIO_ODR_ODR_15) << 1;
	result |= (GPIOG->IDR & GPIO_ODR_ODR_6) << 2;
	result |= (GPIOB->IDR & GPIO_ODR_ODR_4) << 3;
	result |= (GPIOG->IDR & GPIO_ODR_ODR_7) << 4;
	result |= (GPIOI->IDR & GPIO_ODR_ODR_0) << 5;
	result |= (GPIOH->IDR & GPIO_ODR_ODR_6) << 6;
	result |= (GPIOI->IDR & GPIO_ODR_ODR_3) << 7;

	RD_IDLE;
	return result;
}

