#ifndef UTIL_LCD_PIN_UTIL_H_
#define UTIL_LCD_PIN_UTIL_H_
#include "stm32f7xx.h"
#include "gpio.h"
#include "rcc.h"

#define RD_ACTIVE 			GPIOA->BSRR |= GPIO_BSRR_BR_0
#define RD_IDLE				GPIOA->BSRR |= GPIO_BSRR_BS_0
#define WR_ACTIVE  			GPIOF->BSRR |= GPIO_BSRR_BR_10
#define WR_IDLE    			GPIOF->BSRR |= GPIO_BSRR_BS_10
#define CD_COMMAND 			GPIOF->BSRR |= GPIO_BSRR_BR_9
#define CD_DATA    			GPIOF->BSRR |= GPIO_BSRR_BS_9
#define CS_ACTIVE  			GPIOF->BSRR |= GPIO_BSRR_BR_8
#define CS_IDLE    			GPIOF->BSRR |= GPIO_BSRR_BS_8
#define RESET_HIGH			GPIOF->BSRR |= GPIO_BSRR_BS_7
#define RESET_LOW			GPIOF->BSRR |= GPIO_BSRR_BR_7
#define WR_STROBE			WR_ACTIVE; WR_IDLE

void pins_init(void);

void pins_setWrite(void);
void pins_setRead(void);

void pins_write8bits(uint8_t d);
void pins_writeRegister8(uint8_t reg, uint8_t data);
void pins_writeRegister16(uint16_t reg, uint16_t data);
void pins_writeRegister32(uint8_t r, uint32_t d);

uint8_t pins_read8bits(void);


#endif /* UTIL_LCD_PIN_UTIL_H_ */
