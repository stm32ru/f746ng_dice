#ifndef UTIL_LCD_ILI9341_DRIVER_H_
#define UTIL_LCD_ILI9341_DRIVER_H_
#include "stm32f7xx.h"

#define TFT_WIDTH   240
#define TFT_HEIGHT  320

void ILI9341_reset(void);
void ILI9341_init(void);
void ILI9341_drawPixel(int16_t x, int16_t y, uint16_t color);
void ILI9341_setRotation(uint8_t r);


#endif /* UTIL_LCD_ILI9341_DRIVER_H_ */
